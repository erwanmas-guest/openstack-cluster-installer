[network]
# Address of your OpenStack cluster, CIDR notation
OPENSTACK_CLUSTER_NETWORK=192.168.100.0/24

# Name of the NIC that will serve DHCP + PXE
# for installing Debian
PXE_NIC_NAME=osinstallnic0

# User holding the NIC
PXE_VM_NIC_USER=zigo

# name of the tap interface
PXE_VM_VIRTAP_NAME=osinstalltap

# bridge name
PXE_BRIDGE_NAME=osinstallbr

# qemu VM MAC
QEMU_VM_MAC=08:00:27:06:CC:DF

# URL of your debian proxy/mirror
debian_mirror=http://10.4.22.95:9999/debian

# URL of your debian security proxy/mirror
debian_security_mirror=http://10.4.22.95:9999/security

# URL of the incoming buildd repo
debian_incoming_buildd=http://10.4.22.95:9999/debian-buildd

# Ceph repository, needed for Stretch, as we need Luminous from upstream.
# Normal repo without mirroring is: http://download.ceph.com/debian-luminous
# Infomaniak mirror is: http://apt.infomaniak.ch/download.ceph.com/debian-luminous
# Mirror list available at: https://ceph.com/get/#ceph-mirrors
debian_mirror_ceph=http://download.ceph.com/debian-luminous

# Tell if we should use upstream Ceph repo

# Address of the OCI web server that the agent will contact,
# and also the IP address of the puppet-master, as per slave
# nodes /etc/hosts file.
OCI_IP=192.168.111.1

# These are the networks allowed to query the OCI web site
# without authentication, ie: machines that do PXE boot.
# Typically, this is your DHCP network.
# Every network that you will add in OCI will be trusted too,
# so that machines can report their status ie: live, installed, etc..
TRUSTED_NETWORKS=192.168.100.0/24,10.54.240.0/24,10.52.240.0/24

[radius]
# Should the auth system use radius?
use_radius=no

# Address of your Radius authentication server.
server_hostname=localhost

# Shared secret to contact your Radius server.
shared_secret=changeme

[database]
connection=mysql+pymysql://oci:43FAnaQHKizfIrBMksqITw@localhost:3306/oci

[dellipmi]
# If the chassis vendor is Dell, should the Dell iDRAC
# utilities be installed on the target?
target_install_dell_ipmi=no

# Should the Dell iDRAC IPMI utilities be installed on the Live image?
live_image_install_dell_ipmi=no

# Address of the Dell IPMI utilities. At Infomaniak, we
# have a copy of them here:
# http://apt.infomaniak.ch/linux.dell.com/repo/community/debian
dell_ipmi_repo=https://linux.dell.com/repo/community/debian/

[megacli]
# If your vendor is using an LSI card, then you probably
# want to have megacli installed.
target_install_megacli=no

# Should megacli be also installed in the live image?
live_image_install_megacli=no

# Address of the megacli repository. At infomaniak, we use a
# mirror, but you can also use approx like this:
# http://10.4.22.95:9999/hwraid
megacli_repo=http://hwraid.le-vert.net/debian

[live_image]
# In our configuration, the 10 Gbits/s cards are detected first, and
# then, the Debian live tries to do the DHCP on these cards first,
# which then fails. Therefore, a nice hack is to remove these drivers
# from the live image's initrd but leave it within the squashfs. This
# way, the DHCP phase before the wget of the squashfs will not use these
# cards as the driver isn't present.
remove_drivers_from_initrd=no

# List of drivers to remove when building the live image
remove_drivers_from_initrd_list="broadcom/bnx2.ko broadcom/bnx2x/bnx2x.ko intel/ixgbe/ixgbe.ko"

# Syslinux has 2 modes: one which is graphical and displays a nice
# splash screen, and one which is serial console compatible, which
# is text only, without the nice splash screen. Select here which
# one you want to use in the live image.
force_syslinux_text_menu=yes

[releasenames]
# Name of the OpenStack release when building the image
openstack_release=train

# Name of the Debian release when building the image
debian_release=buster

# Should we use a Debian stable unofficial repository?
use_debian_dot_net_backport=yes

# Install official Debian backport repository?
use_debian_official_backports=yes

# Pin Ceph from Debian official backports?
pin_ceph_from_stable_backports=yes

# This is useful for development, when you want fast
# update of the package in the Sid Debian repository.
# Not to be used in production.
install_buildd_incoming=no

install_ceph_upstream_repo=no

[ssh]
# If set to yes, will use the PHP extension
# otherwise, use PHP exec.
use_php_ssh2=no

[ipmi]
# Set this to yes if you want OCI to automatically assign IPMI
# addresses to your machines during the hardware recovery process.
# To do this, a network with role IPMI must be created using:
# ocicli network-create ipmi-network-1 192.168.103.0 24 bdb-zone-1 no
# ocicli network-set ipmi-network-1 --role ipmi --ipmi-match-addr 192.168.100.0 --ipmi-match-cidr 24
# The --ipmi-match-addr and --ipmi-match-cidr must match the DHCP address of discovered machines.
# Note that passwords will be picked-up randomly.
automatic_ipmi_numbering=no

# Set this to the default IPMI username you wish to see
# automatically provisionned
automatic_ipmi_username=oci

[glance_image]
# This image can be used to setup the image automatically in
# the cluster at setup time. Will only be used if it's available.
# This is mostly useful for doing tempest testing automatically.
image_path=/root
image_name=debian-10.3.0-openstack-amd64.qcow2
