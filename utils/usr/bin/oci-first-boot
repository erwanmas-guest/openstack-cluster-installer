#!/bin/sh

set -e

MY_ROLE=$(cat /etc/oci/my-role)

oci-wait-for-networking


start_puppet_service () {
        systemctl enable puppet
        systemctl start puppet
        oci-report-puppet-success
}

# Apply puppet and popuplate PUPPET_RETURN_CODE with the return value.
# Takes the run number as argument.
apply_puppet () {
	local RUN_NUMBER
	RUN_NUMBER=${1}
	# Puppet, with --detailed-exitcodes exits this way:
	# 0: The run succeeded with no changes or failures; the system was already in the desired state.
	# 1: The run failed, or wasn't attempted due to another run already in progress.
	# 2: The run succeeded, and some resources were changed.
	# 4: The run succeeded, and some resources failed.
	# 6: The run succeeded, and included both changes and failures.
	# *: Everything else is undefined.
	# so we run it once, if 0 or 2, success and report back to OCI, otherwise, we try again.
	# If the 2nd run success, we report it, otherwise, report failure.
	set +e
	if [ -e /etc/oci/self-signed-api-cert ] ; then
		OS_CACERT=/etc/ssl/certs/oci-pki-oci-ca-chain.pem puppet agent --test --debug --detailed-exitcodes >/var/log/puppet-run-${RUN_NUMBER} 2>&1
		PUPPET_RETURN_CODE=$?
	else
		puppet agent --test --debug --detailed-exitcodes >/var/log/puppet-run-${RUN_NUMBER} 2>&1
		PUPPET_RETURN_CODE=$?
	fi
	set -e
}

# This function runs puppet multiple times, until applying works.
# It then reports back to OCI if it worked, and start the agent
# service in the system if it worked.
apply_puppet_in_loop () {
	local CNT MAX_RUN
	CNT=0
	MAX_RUN=5

	oci-report-puppet-running

	PUPPET_RETURN_CODE=1
	while [ "${CNT}" -lt "${MAX_RUN}" ] && [ "${PUPPET_RETURN_CODE}" != 0 ] && [ "${PUPPET_RETURN_CODE}" != 2 ]; do
		# Before running the 2nd puppet -t run, we must manually start libvirtd and virtlogd
		if [ ${CNT} = 1 ] && [ "${MY_ROLE}" = "compute" ] ; then
			echo "Before 2nd puppet run: launching oci-fixup-compute-node."
			oci-fixup-compute-node
		fi
		if [ ${CNT} != 0 ] ; then
			echo "Error during the puppet run: running again (run "$((${CNT} + 1))")."
		fi
		CNT=$((${CNT} + 1))
		apply_puppet ${CNT}
	done
	case "${PUPPET_RETURN_CODE}" in
	0|2)
		start_puppet_service
	;;
	*)
		oci-report-puppet-failure
	;;
	esac
}

if [ -e /var/lib/oci-first-boot ] ; then
	rm -f /var/lib/oci-first-boot
	case ${MY_ROLE} in
	"compute")
		if [ -e /dev/sdb ] ; then
			oci-build-nova-instances-vg /dev/sdb
		fi
		if [ -e /dev/vdb ] ; then
			oci-build-nova-instances-vg /dev/vdb
		fi
		oci-write-lvm-filter
		oci-fix-nova-ssh-config
	;;
	"volume")
		oci-write-lvm-filter
		oci-build-cinder-volume-vg
	;;
	*)
		echo -n ""
	;;
	esac

	apply_puppet_in_loop
fi

exit 0
