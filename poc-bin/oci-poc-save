#!/bin/sh

# EXPERIMENTAL: never tested

set -e

usage () {
	echo "$0 <CLUSTER-NAME>"
	echo "This command saves the state of a running PoC cluster."
	echo "See oci-poc-restore to restore the state of a cluster."
	exit 1
}

. /etc/oci-poc/oci-poc.conf

# Wait until a PID file is gone, meaning the VM has stopped.
# Do so with a timeout, until wich we kill the PID.
wait_for_pidfile_gone () {
	local PID_FILE TIMEOUT
	PID_FILE=${1}
	TIMEOUT=${2}

	CNT=${TIMEOUT}
	while [ -e ${PID_FILE} -a ${CNT} -gt 0 ] ; do
		CNT=$((${CNT} - 1))
	done
	if [ -e ${PID_FILE} ] ; then
		echo "Killing VM"
		kill $(cat ${PID_FILE})
		rm -f ${PID_FILE}
	else
		echo "VM is gone"
	fi
}

if [ "${#}" != 1 ] || [ "${1}" = "-h" ] || [ "${1}" = "--help" ] || [ "${1}" = "-help" ] ; then
	usage
fi

CLUSTER_NAME=${1}

shutdown_mysql_servers () {
	echo "===> Turning off MySQL"
	for i in $(ocicli -csv machine-list -a | q -H -d, "SELECT Cur_ip FROM - WHERE status='installed' AND role='controller'" | sort -r) ; do
		ssh $i "/etc/init.d/mysql stop"
		sleep 5
	done
}

shutdown_all_installed_vms () {
	echo "===> Turning installed VMs off..."
	for i in $(ocicli -csv machine-list -a | q -H -d, "SELECT Cur_ip FROM - WHERE status='installed'") ; do
		HOSTNAME=$(ocicli -csv machine-list | q -H -d, "SELECT hostname FROM - WHERE Cur_ip='${i}'")
		echo "Shutting down: ${HOSTNAME}"
		timeout 3 ssh root@${i} "shutdown -h now" || true
	done

	for i in $(ocicli -csv machine-list -a | q -H -d, "SELECT Cur_ip FROM - WHERE status='installed'") ; do
		HOSTNAME=$(ocicli -csv machine-list | q -H -d, "SELECT hostname FROM - WHERE Cur_ip='${i}'")
		SERIAL=$(ocicli -csv machine-list | q -H -d, "SELECT serial FROM - WHERE Cur_ip='${i}'")
		VM_NUM=$(printf "%d" $((0x${SERIAL} - 0xC0)))
		PID_FILE=/var/run/oci-poc/slave-node-${VM_NUM}.pid
		echo "Waiting for host to be down: ${HOSTNAME}"
		wait_for_pidfile_gone ${PID_FILE} 120
	done

	timeout 5 ssh root@${HOST_NETWORK_PREFIX}.2 "shutdown -h now" || true
	wait_for_pidfile_gone /var/run/oci-poc/pxe-server-node.pid 60
}

kill_remaining_processes () {
	echo "===> Killing not-installed VMs..."
	VMS_PID_FILES=$(ls /var/run/oci-poc/slave-node-*.pid | grep -v ipmisim | tr '\n' ' ')
	for i in ${VMS_PID_FILES} ; do
		kill $(cat $i) || true
		rm -f $i
	done
	echo "===> Killing all IPMI SIM..."
	IPMI_SIM_PID_FILES=$(ls /var/run/oci-poc/slave-node-*.ipmisim.pid | tr '\n' ' ')
	for i in ${IPMI_SIM_PID_FILES} ; do
		kill $(cat $i) || true
		rm -f $i
	done
	kill $(cat /var/run/oci-poc/pxe-server-node.pid.ipmisim.pid) || true
	rm -f /var/run/oci-poc/pxe-server-node.pid.ipmisim.pid
}
make_backup_of_files () {
	echo "===> Making backup of .qcow2 files..."
	mkdir -p /var/lib/openstack-cluster-installer-poc/saved/${CLUSTER_NAME}
	cp -axufv /var/lib/openstack-cluster-installer-poc/runtime/* /var/lib/openstack-cluster-installer-poc/saved/${CLUSTER_NAME}/

	echo "===> Making backup of ipmi_sim config files..."
	cp -axuf /var/lib/openstack-cluster-installer-poc/ipmi_sim/*.conf /var/lib/openstack-cluster-installer-poc/saved/${CLUSTER_NAME}/
}


shutdown_mysql_servers
shutdown_all_installed_vms
kill_remaining_processes
make_backup_of_files
