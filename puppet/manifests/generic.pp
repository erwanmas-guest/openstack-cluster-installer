class oci::generic(
#  $machine_hostname         = undef,
#  $machine_ip               = undef,
  $ssh_listen_ips           = ['127.0.0.0', "${::fqdn}"],
  $etc_hosts                = undef,
  $time_server_host         = undef,
  $pass_root_ssh_pub        = undef,
  $pass_root_ssh_priv       = undef,
  $authorized_keys_hash     = undef,
#  $time_server_use_pool     = false,
){
  # Generic sysctl options
  ::oci::sysctl { 'oci-rox': }

  # Populate /etc/hosts
  class { '::oci::etchosts':
    etc_hosts_file => $etc_hosts,
  }

  ::oci::oci_ssh_keypair { 'root-keypair':
    path    => '/root',
    type    => 'ssh-rsa',
    user    => 'root',
    group   => 'root',
    pubkey  => $pass_root_ssh_pub,
    privkey => base64('decode', $pass_root_ssh_priv),
  }

  if $authorized_keys_hash {
    $authorized_keys_hash.map |String $key_hostname, String $key_pubkey| {
      ssh_authorized_key { "oci-auto-authorized-keys-hash-${key_hostname}":
        ensure  => present,
        key     => $key_pubkey,
        type    => 'ssh-rsa',
        user    => 'root',
      }
    }
  }

  # Setup chrony
  class {
    '::oci::chrony': time_server_host => $time_server_host,
  }

  # Make sure we have anacron, because we may install some
  # cron on nodes.
  package { 'anacron':
    ensure => present,
  }

  # Fix-up iptables-legacy as selected alternative
  if $facts['os']['lsb'] != undef{
    $mycodename = $facts['os']['lsb']['distcodename']
  }else{
    $mycodename = $facts['os']['distro']['codename']
  }

  if $mycodename != 'stretch'{
    alternatives { 'iptables':
      path => '/usr/sbin/iptables-legacy',
    }
    alternatives { 'ip6tables':
      path => '/usr/sbin/ip6tables-legacy',
    }
  }

  # Fix-up the /etc/ssh/sshd_config to be safer, ie
  # listen only on localhost & management network,
  # avoiding the risk that ssh binds on public IP,
  # also make sure we're using the signed ssh host keys.
  #
  # Appart from ListenAddress and HostCertificate,
  # this is the default configuration from Debian.
  class { 'ssh::server':
    options => {
      'Port'                            => [22],
      'ListenAddress'                   => $ssh_listen_ips,
      'ChallengeResponseAuthentication' => 'no',
      'UsePAM'                          => 'yes',
      'X11Forwarding'                   => 'yes',
      'PrintMotd'                       => 'no',
      'AcceptEnv'                       => 'LANG LC_*',
      'Subsystem'                       => 'sftp /usr/lib/openssh/sftp-server',
      'HostCertificate'                 => $facts['oci_ssh_host_key_certs_list'],
      #['/etc/ssh/ssh_host_ecdsa_key-cert.pub', '/etc/ssh/ssh_host_ed25519_key-cert.pub', '/etc/ssh/ssh_host_rsa_key-cert.pub']
    }
  }
}
