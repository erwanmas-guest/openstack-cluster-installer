class oci::etcddiscovery(
  $prefix_url             = 'http://example.com',
  $web_service_address    = ':8087',
  $etcd_endpoint_location = 'http://127.0.0.1:2379',
){

  package { 'etcd-discovery':
    ensure => 'present',
  }->
  file { '/etc/etcd-discovery/etcd-discovery.conf':
    ensure => 'present',
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => "# This file is managed by puppet: do not touch

[server]
# used as prefix when returning the cluster ID
prefix_url=${prefix_url}

web_service_address=${web_service_address}

[etcd]
# Where is located your ETCD cluster?
endpoint_location=${etcd_endpoint_location}
"
  }->
  service { 'etcd-discovery':
    ensure    => 'running',
    enable    => true,
    hasstatus => true,
  }
}
