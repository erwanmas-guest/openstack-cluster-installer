# Setup a machine with Tempest to do testing of the cluster
class oci::tempest(
  $openstack_release        = undef,
  $cluster_name             = undef,
  $machine_hostname         = undef,
  $machine_ip               = undef,

  $vip_hostname             = undef,
  $self_signed_api_cert     = true,

  $cluster_has_cinder       = false,
  $cluster_has_ceph         = false,
  $cluster_has_compute      = false,
  $cluster_has_swift        = false,

  $pass_keystone_adminuser  = undef,
){

  $proto = 'https'
  $messaging_default_port = '5671'
  $messaging_notify_port = '5671'
  $api_port = 443

  $base_url = "${proto}://${vip_hostname}"
  if $self_signed_api_cert {
    $api_endpoint_ca_file = "/etc/ssl/certs/oci-pki-oci-ca-chain.pem"
  }else{
    $api_endpoint_ca_file = ''
  }
  $oci_pki_root_ca_file = '/etc/ssl/certs/oci-pki-oci-ca-chain.pem'

  class { '::oci::puppet_oci_ca_cert':
    self_signed_api_cert => $self_signed_api_cert,
  }

  $keystone_auth_uri  = "${base_url}:${api_port}/identity"
  $keystone_admin_uri = "${base_url}:${api_port}/identity"

  ensure_resource('file', '/root/oci-openrc', {
    'ensure'  => 'present',
    'content' => "
export OS_AUTH_TYPE=password
export OS_PROJECT_DOMAIN_NAME='default'
export OS_USER_DOMAIN_NAME='default'
export OS_PROJECT_NAME='admin'
export OS_USERNAME='admin'
export OS_PASSWORD='${pass_keystone_adminuser}'
export OS_AUTH_URL='${base_url}/identity/v3'
export OS_IDENTITY_API_VERSION=3
export OS_IMAGE_API_VERSION=2
export OS_CACERT=${$api_endpoint_ca_file}
",
    'mode'    => '0640',
  })

  class { '::tempest':
    package_ensure                   => 'present',
    tempest_workspace                => '/var/lib/tempest',
    install_from_source              => false,
    git_clone                        => false,
    tempest_config_file              => '/etc/tempest/tempest.conf',
    setup_venv                       => false,

    # Glance image config
    #
    configure_images                 => true,
    image_name                       => 'debian-10.3.0-openstack-amd64.qcow2',
    image_name_alt                   => 'debian-10.3.0-openstack-amd64.qcow2',
    glance_v1                        => false,
    glance_v2                        => true,

    # Neutron network config
    #
    configure_networks               => true,
    public_network_name              => 'ext-net',
    neutron_api_extensions           => [
      'address-scope',
      'agent',
      'allowed-address-pairs',
      'auto-allocated-topology',
      'availability_zone',
      'binding',
      'default-subnetpools',
      'dhcp_agent_scheduler',
      'dvr',
      'ext-gw-mode,external-net',
      'extra_dhcp_opt',
      'extraroute',
      'flavors',
      'l3-flavors',
      'l3-ha',
      'l3_agent_scheduler',
      'metering',
      'net-mtu',
      'network-ip-availability',
      'network_availability_zone',
      'pagination',
      'project-id',
      'provider',
      'quotas',
      'rbac-policies',
      'router',
      'router_availability_zone',
      'security-group',
      'service-type',
      'sorting',
      'standard-attr-description',
      'standard-attr-revisions',
      'standard-attr-timestamp',
      'subnet_allocation',
      'tag',
      'tag-ext',
    ],

    # Horizon dashboard config
    login_url                        => "${base_url}/horizon",
    dashboard_url                    => "${base_url}/horizon",
    disable_dashboard_ssl_validation => true,

    # tempest.conf parameters
    #
    identity_uri_v3                  => "${keystone_auth_uri}/v3",
    cli_dir                          => '/usr/bin',
    lock_path                        => '/var/lib/tempest',
    log_file                         => '/var/lib/tempest/tempest-log.log',
    debug                            => true,
    use_stderr                       => true,
    use_syslog                       => false,
    logging_context_format_string    => $::os_service_default,
    attach_encrypted_volume          => false,

    # admin user
    admin_username                   => 'admin',
    admin_password                   => $pass_keystone_adminuser,
    admin_project_name               => 'admin',
    admin_role                       => 'admin',
    admin_domain_name                => 'Default',

    # roles fo the users created by tempest
    tempest_roles                    => ['member', 'creator'],

    # image information
    image_ssh_user                   => 'debian',
    image_alt_ssh_user               => 'debian',
    flavor_ref                       => 'demo-flavor',
    flavor_ref_alt                   => 'cpu2-ram12-disk20',
    compute_build_interval           => 10,
    run_ssh                          => true,

    # testing features that are supported
    resize_available                 => false,
    change_password_available        => false,

    # Service configuration
    cinder_available                 => $cluster_has_cinder,
    cinder_backup_available          => false,
    glance_available                 => true,
    heat_available                   => true,
    ceilometer_available             => $cluster_has_ceph,
    aodh_available                   => $cluster_has_ceph,
    gnocchi_available                => $cluster_has_ceph,
    panko_available                  => $cluster_has_ceph,
    designate_available              => false,
    horizon_available                => true,
    neutron_available                => true,
    neutron_bgpvpn_available         => false,
    neutron_l2gw_available           => false,
    neutron_vpnaas_available         => false,
    neutron_dr_available             => false,
    nova_available                   => $cluster_has_compute,
    murano_available                 => false,
    sahara_available                 => false,
    swift_available                  => $cluster_has_swift,
    trove_available                  => false,
    ironic_available                 => false,
    watcher_available                => false,
    zaqar_available                  => false,
    ec2api_available                 => false,
    mistral_available                => false,
    vitrage_available                => false,
    congress_available               => false,
    octavia_available                => $cluster_has_compute,
    barbican_available               => true,
    keystone_v2                      => false,
    keystone_v3                      => true,
    auth_version                     => 'v3',
    run_service_broker_tests         => false,
    ca_certificates_file             => $api_endpoint_ca_file,
    disable_ssl_validation           => true,
    manage_tests_packages            => true,
    # scenario options
    img_dir                          => '/root',
    img_file                         => 'cirros-0.4.0-x86_64-disk.img',
  }
#  tempest_config {
#    'compute-feature-enabled/block_migration_for_live_migration': value => true;
#    'compute-feature-enabled/rescue':                             value => false;
#    'network/floating_network_name':                              value => 'ext-net';
#    'network-feature-enabled/ipv6':                               value => false;
#  }
}
