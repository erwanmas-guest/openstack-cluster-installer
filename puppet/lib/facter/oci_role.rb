# module_name/lib/facter/oci_role.rb
Facter.add(:oci_role) do
  setcode do
    myrole = 'default'

    if File.exist? '/etc/oci/my-role'
      myrole = File.read('/etc/oci/my-role')
    end

    myrole.chomp

  end
end
